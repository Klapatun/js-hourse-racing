

const {
  showHorses,
  showAccount,
  setWager,
  startRacing,
  newGame
} = (()=> {

  let endOfRace = false;

  const run = function() {
    return new Promise((resolve) => {
      let time = (Math.random() * (3000-500))+500;
      setTimeout(resolve, time, { name: this.name, time: Math.round(time) });
    });
  };

  const horses = [
    {
      name: 'Roger',
      run: run
    },
    {
      name: 'Forest',
      run: run
    },
    {
      name: 'George',
      run: run
    },
    {
      name: 'Ron',
      run: run
    },
    {
      name: 'Decster',
      run: run
    },
  ];

  const player = {
    account: 100,
    wagerArr: [],
    setWager: function(horseName, wager) {
      
      if(!Number.isInteger(wager)) {
        console.log('Wager should be intager');
        return;
      }

      let hasHorse = false;
      horses.forEach((horse)=>{
        if(horse.name === horseName) {
          hasHorse = true;
          return;
        }
      });

      if(!hasHorse) {
        console.log('Sorry, we dont have this horse');
        return;
      }

      //Проверяем кошелек
      if(this.account - wager < 0) {
        console.log('Not enough money');
        return;
      }

      //Вытаскиваем деньги
      this.account -= wager;

      //Заносим объект ставки в массив
      this.wagerArr.push({
        horse: horseName,
        yourWager: wager
      });
    },

    checkWagers: function(winnerName) {
      let gain = 0;

      this.wagerArr.forEach(obj=> {
        if(obj.horse === winnerName) {
          gain += obj.yourWager * 2;
          this.account += obj.yourWager * 2;
        }
      });
      console.log(`Вы выйграли ${gain}, текущий счет: ${this.account}`);
    }
  };

  return {
    showHorses: function() {
      horses.forEach(horse=>{
        console.log(horse.name);
      });
    },

    showAccount: function() {
      console.log(player.account);
    },

    setWager: function(horseName, wager) {
      if(endOfRace) {
        console.log('Please, start new race. For it you need enter "newGame()"');
        return;
      }
      player.setWager(horseName, wager);
    },

    //Я не понял как это можно организовать вместе с Promise.race
    startRacing: function() {

      if(endOfRace) {
        console.log('Please, start new race. For it you need enter "newGame()"');
        return;
      }

      let winner;

      const HorseRace = horses.map(horse=>horse.run());

      Promise.race(HorseRace).then((response)=>{
        winner = response;
      });

      Promise.all(HorseRace).then(response=>{
        response.map(horse=>console.log(`Финишь лошади ${horse.name}. Время: ${horse.time}`));
        console.log(`Победитель: ${winner.name}!!!`);
        player.checkWagers(winner.name);
        player.wagerArr.length = 0;
      });

      endOfRace = true;

    },

    newGame: function() {
      console.clear();

      endOfRace = false;
    }
  };
})();
